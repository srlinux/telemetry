#! /bin/bash

for srl_name in $(docker ps | grep containerlab | awk '{print $14}'); do
    # Populate /etc/hosts file
    srl_ip=$(docker inspect $srl_name --format "{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}")
    sed -i "/${srl_name}/d" /etc/hosts
    echo "${srl_ip} ${srl_name}" >> /etc/hosts
done

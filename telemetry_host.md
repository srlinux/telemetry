# Prepare host system

## Host Network
Containerlab requires a few Linux Bridges to be created
  >Script: [00.create_networks.sh](00.create_networks.sh)

  ```sh
  nmcli connection add type bridge autoconnect yes con-name br-leaf1-36 ifname br-leaf1-36
  nmcli connection add type bridge autoconnect yes con-name br-leaf2-36 ifname br-leaf2-36
  nmcli connection add type bridge autoconnect yes con-name br-leaf3-36 ifname br-leaf3-36
  nmcli connection show
  ip link set dev br-leaf1-36 mtu 9212
  ip link set dev br-leaf2-36 mtu 9212
  ip link set dev br-leaf3-36 mtu 9212
  ```
Note: Linux bridge in general simplifies a test tool connectivity. 
It could be:
  - external HW test tool (Spirent, Ixia)
  - virtual test tool (Spirent, Ixia)
  - containerised test tool
    - iperf3 is used in this particular demo

## Users, Directories, Permissions

All telemetry containers are run on behalf of "telemetry" user. 
Proper permissions should be also set to all working subdirectories.
  >Script: [00.create_user.sh](00.create_user.sh)

  ```sh
  useradd -rs /bin/false telemetry
  mkdir -p $(pwd)/telemetry/prometheus
  mkdir -p $(pwd)/telemetry/grafana
  chown -R telemetry:telemetry $(pwd)/telemetry/prometheus
  chown -R telemetry:telemetry $(pwd)/telemetry/grafana
  ```

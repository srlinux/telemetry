# SRLinux Telemetry demo
## Introduction

This project is a work in progress to demonstrate a basic telemetry functionality for Nokia SRLinux products.

## Requirements

Host system 
- Centos8

Internet access is reauired to pull docker images.

The following tools have to be installed on the host system:
- docker-ce
- containerlab (https://containerlab.srlinux.dev/install/)
- gnmic (https://gnmic.kmrd.dev/#installation)

The following docker images are used:
- SRLinux (provided by Nokia)
  - license.key file (provided by Nokia)
- Telegraf
- Prometheus
- Grafana
- (optionally) any container with "iperf3"

## Preparation phases
- [Prepare host system](telemetry_host.md)
- [Deploy SRLinux fabric](telemetry_fabric.md)
- [Deploy tools container](telemetry_tools.md)

## Demo phases
- [Telemetry: SRL local CLI](telemetry_cli.md)
- [Telemetry: Linux tools (gNMIc)](telemetry_gnmi.md)
- [Telemetry: Telegraf/Prometheus/Grafana](telemetry_grafana.md)

## Roadmap
- SRL configuration depoyment using Ansible
- Ubuntu support

#! /bin/bash

useradd -rs /bin/false telemetry
mkdir -p $(pwd)/telemetry/prometheus
mkdir -p $(pwd)/telemetry/grafana
chown -R telemetry:telemetry $(pwd)/telemetry/prometheus
chown -R telemetry:telemetry $(pwd)/telemetry/grafana

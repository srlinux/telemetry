#! /bin/bash

user='978:977'
docker run -d \
    --user $user \
    --name=telegraf \
    --net=host \
    -e HOST_PROC=/host/proc \
    -v /proc:/host/proc:ro \
    -v $(pwd)/telemetry/telegraf.conf:/etc/telegraf/telegraf.conf:ro \
    telegraf

docker run -d \
    --user $user \
    --name=prometheus \
    --net=host \
    -v $(pwd)/telemetry/prometheus.yml:/etc/prometheus/prometheus.yml \
    -v $(pwd)/telemetry/prometheus:/data/prometheus \
    prom/prometheus \
    --config.file="/etc/prometheus/prometheus.yml" \
    --storage.tsdb.path="/data/prometheus"

docker run -d \
    --user $user \
    --net=host \
    --name=grafana \
    -v "$(pwd)/telemetry/grafana:/var/lib/grafana" \
    -p 3000:3000 \
    grafana/grafana

# Telemetry: gNMIc tool
'gnmic' is a gNMI CLI client that provides full support for Capabilities, Get, Set and Subscribe RPCs with collector capabilities.
  > More information here: https://gnmic.kmrd.dev/

## gNMIc configuration
- Minimal configuration file. Inspired by https://github.com/karimra/gnmic/blob/master/config.yaml
  > file: [gnmic_subscription.yaml](gnmic_subscription.yaml)
  
    ```yml
    targets:
      containerlab-t-spine1:
      containerlab-t-leaf1:

    username: admin
    password: admin
    port: 57400
    timeout: 10s
    tls-cert: certs/client.cert.pem
    tls-ca: certs/ca.cert.pem
    #skip-verify: false
    skip-verify: true
    encoding: json_ietf
    #debug: true
    no-prefix: true
    ```

## gNMIc examples
### Single XPath subscription
- Subscribe command
    ```sh
    gnmic \
        --config gnmic_subscription.yaml \
        subscribe \
        --path /network-instance[name=*]/protocols/bgp/neighbor[peer-address=*]/received-messages
    ```
    <details>
    <summary>Click to see output</summary>    
        
    ```json
    # gnmic --config gnmic_subscription.yaml subscribe --path /network-instance[name=*]/protocols/bgp/neighbor[peer-address=*]/received-messages
        
        {
          "source": "containerlab-t-leaf1:57400",
          "subscription-name": "default",
          "timestamp": 1602191309231685332,
          "time": "2020-10-08T23:08:29.231685332+02:00",
          "updates": [
            {
              "Path": "srl_nokia-network-instance:network-instance[name=default]/protocols/srl_nokia-bgp:bgp/neighbor[peer-address=2002::100:64:1:0]/received-messages",
              "values": {
                "srl_nokia-network-instance:network-instance/protocols/srl_nokia-bgp:bgp/neighbor/received-messages": {
                  "malformed-updates": "0",
                  "queue-depth": 0,
                  "total-messages": "5819",
                  "total-non-updates": "5807",
                  "total-updates": "12"
                }
              }
            }
          ]
        }
        {
          "source": "containerlab-t-spine1:57400",
          "subscription-name": "default",
          "timestamp": 1602191309232422682,
          "time": "2020-10-08T23:08:29.232422682+02:00",
          "updates": [
            {
              "Path": "srl_nokia-network-instance:network-instance[name=default]/protocols/srl_nokia-bgp:bgp/neighbor[peer-address=2002::100:64:2:1]/received-messages",
              "values": {
                "srl_nokia-network-instance:network-instance/protocols/srl_nokia-bgp:bgp/neighbor/received-messages": {
                  "malformed-updates": "0",
                  "queue-depth": 0,
                  "total-messages": "5827",
                  "total-non-updates": "5817",
                  "total-updates": "10"
                }
              }
            }
          ]
        }
        {
          "source": "containerlab-t-spine1:57400",
          "subscription-name": "default",
          "timestamp": 1602191309233279016,
          "time": "2020-10-08T23:08:29.233279016+02:00",
          "updates": [
            {
              "Path": "srl_nokia-network-instance:network-instance[name=default]/protocols/srl_nokia-bgp:bgp/neighbor[peer-address=2002::100:64:2:3]/received-messages",
              "values": {
                "srl_nokia-network-instance:network-instance/protocols/srl_nokia-bgp:bgp/neighbor/received-messages": {
                  "malformed-updates": "0",
                  "queue-depth": 0,
                  "total-messages": "5822",
                  "total-non-updates": "5816",
                  "total-updates": "6"
                }
              }
            }
          ]
        }
        
        {
          "source": "containerlab-t-leaf1:57400",
          "subscription-name": "default",
          "timestamp": 1602191309233513344,
          "time": "2020-10-08T23:08:29.233513344+02:00",
          "updates": [
            {
              "Path": "srl_nokia-network-instance:network-instance[name=default]/protocols/srl_nokia-bgp:bgp/neighbor[peer-address=2002::100:64:2:0]/received-messages",
              "values": {
                "srl_nokia-network-instance:network-instance/protocols/srl_nokia-bgp:bgp/neighbor/received-messages": {
                  "malformed-updates": "0",
                  "queue-depth": 0,
                  "total-messages": "5808",
                  "total-non-updates": "5800",
                  "total-updates": "8"
                }
              }
            }
          ]
        }
        {
          "source": "containerlab-t-leaf1:57400",
          "subscription-name": "default",
          "timestamp": 1602191309234100903,
          "time": "2020-10-08T23:08:29.234100903+02:00",
          "updates": [
            {
              "Path": "srl_nokia-network-instance:network-instance[name=default]/protocols/srl_nokia-bgp:bgp/neighbor[peer-address=2002::100:64:3:0]/received-messages",
              "values": {
                "srl_nokia-network-instance:network-instance/protocols/srl_nokia-bgp:bgp/neighbor/received-messages": {
                  "malformed-updates": "0",
                  "queue-depth": 0,
                  "total-messages": "5806",
                  "total-non-updates": "5802",
                  "total-updates": "4"
                }
              }
            }
          ]
        }
        
    ```
###  Multiple XPath subscriptions
- Multiple XPath subscriptions are possible if 'subscriptions' container is defined in the configuration file
  - Here is an examples of different subscription modes (more information could be found on the official project page)
    - stream/sample
    - stream/on-change
    - once
    ```yml
      # SRLinux subscription container
      subscriptions:
          port_stats:
              paths:
                - "/interface[name=ethernet-1/1]/traffic-rate"
                - "/interface[name=ethernet-1/2]/traffic-rate"
              mode: stream
              stream-mode: sample
              sample-interval: 5s
              encoding: json_ietf
          bgp_neighbor_state:
              paths:
                - "network-instance[name=default]/protocols/bgp/neighbor[peer-address=200*]/admin-state"
              mode: stream
              stream-mode: on-change
          service_state:
              paths:
                - "/network-instance[name=*]/admin-state"
              mode: stream
              stream-mode: on-change
          system_facts:
              paths:
                - "/system/name/host-name"
                - "/platform/control[slot=*]/software-version"
              mode: once
    ```
  - Then a 'subscriptions' could be allocated for a particular target(s) usign the following format: 
      ```yml
      targets:
          containerlab-t-leaf1:
              subscriptions:
                  - system_facts
                  - port_stats
                  - service_state
                  - bgp_neighbor_state
      ```
      <details>
      <summary>Click to see full configuration</summary>
      
      ```yml
        # cat gnmic_subscription.yaml
        # Inspired by https://github.com/karimra/gnmic/blob/master/config.yaml

        targets:
          containerlab-t-spine1:
          containerlab-t-leaf1:
              subscriptions:
              - system_facts
              - port_stats
              - service_state
              - bgp_neighbor_state

        username: admin
        password: admin
        port: 57400
        timeout: 10s
        tls-cert: certs/client.cert.pem
        tls-ca: certs/ca.cert.pem
        #skip-verify: false
        skip-verify: true
        encoding: json_ietf
        #debug: true
        no-prefix: true

        # SRLinux subscription container
        subscriptions:
          port_stats:
              paths:
                - "/interface[name=ethernet-1/1]/traffic-rate"
                - "/interface[name=ethernet-1/2]/traffic-rate"
              mode: stream
              stream-mode: sample
              sample-interval: 5s
              encoding: json_ietf
          bgp_neighbor_state:
              paths:
                - "network-instance[name=default]/protocols/bgp/neighbor[peer-address=200*]/admin-state"
              mode: stream
              stream-mode: on-change
          service_state:
              paths:
                - "/network-instance[name=*]/admin-state"
              mode: stream
              stream-mode: on-change
          system_facts:
              paths:
                - "/system/name/host-name"
                - "/platform/control[slot=*]/software-version"
              mode: once
        ```  
       </details>

  - Start subscription
      ``` 
      gnmic --config gnmic_subscription.yaml subscribe 
      ```
      <details>
      <summary>Click to see output</summary>    
          
      ```json
      # gnmic --config gnmic_subscription.yaml subscribe

          {
          "source": "containerlab-t-spine1:57400",
          "subscription-name": "bgp_neighbor_state",
          "timestamp": 1602192196556196385,
          "time": "2020-10-08T23:23:16.556196385+02:00",
          "updates": [
              {
              "Path": "srl_nokia-network-instance:network-instance[name=default]/protocols/srl_nokia-bgp:bgp/neighbor[peer-address=2002::100:64:2:1]",
              "values": {
                  "srl_nokia-network-instance:network-instance/protocols/srl_nokia-bgp:bgp/neighbor": {
                  "admin-state": "enable"
                  }
              }
              }
          ]
          }
          {
          "source": "containerlab-t-spine1:57400",
          "subscription-name": "bgp_neighbor_state",
          "timestamp": 1602192196557020192,
          "time": "2020-10-08T23:23:16.557020192+02:00",
          "updates": [
              {
              "Path": "srl_nokia-network-instance:network-instance[name=default]/protocols/srl_nokia-bgp:bgp/neighbor[peer-address=2002::100:64:2:3]",
              "values": {
                  "srl_nokia-network-instance:network-instance/protocols/srl_nokia-bgp:bgp/neighbor": {
                  "admin-state": "enable"
                  }
              }
              }
          ]
          }

          {
          "source": "containerlab-t-leaf1:57400",
          "subscription-name": "port_stats",
          "timestamp": 1602192196565092631,
          "time": "2020-10-08T23:23:16.565092631+02:00",
          "updates": [
              {
              "Path": "srl_nokia-interfaces:interface[name=ethernet-1/1]/traffic-rate",
              "values": {
                  "srl_nokia-interfaces:interface/traffic-rate": {
                  "in-bps": "87",
                  "out-bps": "71"
                  }
              }
              }
          ]
          }
          {
          "source": "containerlab-t-spine1:57400",
          "subscription-name": "service_state",
          "timestamp": 1602192196566517146,
          "time": "2020-10-08T23:23:16.566517146+02:00",
          "updates": [
              {
              "Path": "srl_nokia-network-instance:network-instance[name=mgmt]",
              "values": {
                  "srl_nokia-network-instance:network-instance": {
                  "admin-state": "enable"
                  }
              }
              }
          ]
          }
          {
          "source": "containerlab-t-spine1:57400",
          "subscription-name": "service_state",
          "timestamp": 1602192196566327914,
          "time": "2020-10-08T23:23:16.566327914+02:00",
          "updates": [
              {
              "Path": "srl_nokia-network-instance:network-instance[name=default]",
              "values": {
                  "srl_nokia-network-instance:network-instance": {
                  "admin-state": "enable"
                  }
              }
              }
          ]
          }
          {
          "source": "containerlab-t-leaf1:57400",
          "subscription-name": "system_facts",
          "timestamp": 1602192196566403607,
          "time": "2020-10-08T23:23:16.566403607+02:00",
          "updates": [
              {
              "Path": "srl_nokia-system:system/srl_nokia-system-name:name/host-name",
              "values": {
                  "srl_nokia-system:system/srl_nokia-system-name:name/host-name": "spine1"
              }
              }
          ]
          }
          {
          "source": "containerlab-t-leaf1:57400",
          "subscription-name": "port_stats",
          "timestamp": 1602192196566686600,
          "time": "2020-10-08T23:23:16.5666866+02:00",
          "updates": [
              {
              "Path": "srl_nokia-interfaces:interface[name=ethernet-1/2]/traffic-rate",
              "values": {
                  "srl_nokia-interfaces:interface/traffic-rate": {
                  "in-bps": "159",
                  "out-bps": "159"
                  }
              }
              }
          ]
          }
          {
          "source": "containerlab-t-leaf1:57400",
          "subscription-name": "service_state",
          "timestamp": 1602192196566917856,
          "time": "2020-10-08T23:23:16.566917856+02:00",
          "updates": [
              {
              "Path": "srl_nokia-network-instance:network-instance[name=default]",
              "values": {
                  "srl_nokia-network-instance:network-instance": {
                  "admin-state": "enable"
                  }
              }
              }
          ]
          }
          {
          "source": "containerlab-t-spine1:57400",
          "subscription-name": "port_stats",
          "timestamp": 1602192196566919513,
          "time": "2020-10-08T23:23:16.566919513+02:00",
          "updates": [
              {
              "Path": "srl_nokia-interfaces:interface[name=ethernet-1/1]/traffic-rate",
              "values": {
                  "srl_nokia-interfaces:interface/traffic-rate": {
                  "in-bps": "159",
                  "out-bps": "159"
                  }
              }
              }
          ]
          }
          {
          "source": "containerlab-t-spine1:57400",
          "subscription-name": "system_facts",
          "timestamp": 1602192196567001411,
          "time": "2020-10-08T23:23:16.567001411+02:00",
          "updates": [
              {
              "Path": "srl_nokia-system:system/srl_nokia-system-name:name/host-name",
              "values": {
                  "srl_nokia-system:system/srl_nokia-system-name:name/host-name": "leaf2"
              }
              }
          ]
          }



          {
          "source": "containerlab-t-leaf1:57400",
          "subscription-name": "service_state",
          "timestamp": 1602192196567822663,
          "time": "2020-10-08T23:23:16.567822663+02:00",
          "updates": [
              {
              "Path": "srl_nokia-network-instance:network-instance[name=mgmt]",
              "values": {
                  "srl_nokia-network-instance:network-instance": {
                  "admin-state": "enable"
                  }
              }
              }
          ]
          }

          {
          "source": "containerlab-t-leaf1:57400",
          "subscription-name": "bgp_neighbor_state",
          "timestamp": 1602192196567762305,
          "time": "2020-10-08T23:23:16.567762305+02:00",
          "updates": [
              {
              "Path": "srl_nokia-network-instance:network-instance[name=default]/protocols/srl_nokia-bgp:bgp/neighbor[peer-address=2002::100:64:1:0]",
              "values": {
                  "srl_nokia-network-instance:network-instance/protocols/srl_nokia-bgp:bgp/neighbor": {
                  "admin-state": "enable"
                  }
              }
              }
          ]
          }
          {
          "source": "containerlab-t-leaf1:57400",
          "subscription-name": "system_facts",
          "timestamp": 1602192196568304778,
          "time": "2020-10-08T23:23:16.568304778+02:00",
          "updates": [
              {
              "Path": "srl_nokia-platform:platform/srl_nokia-platform-control:control[slot=A]",
              "values": {
                  "srl_nokia-platform:platform/srl_nokia-platform-control:control": {
                  "software-version": "v20.6.1-286-g118bc27b34"
                  }
              }
              }
          ]
          }
          {
          "source": "containerlab-t-spine1:57400",
          "subscription-name": "port_stats",
          "timestamp": 1602192196568507068,
          "time": "2020-10-08T23:23:16.568507068+02:00",
          "updates": [
              {
              "Path": "srl_nokia-interfaces:interface[name=ethernet-1/2]/traffic-rate",
              "values": {
                  "srl_nokia-interfaces:interface/traffic-rate": {
                  "in-bps": "71",
                  "out-bps": "87"
                  }
              }
              }
          ]
          }
          {
          "source": "containerlab-t-spine1:57400",
          "subscription-name": "system_facts",
          "timestamp": 1602192196568689827,
          "time": "2020-10-08T23:23:16.568689827+02:00",
          "updates": [
              {
              "Path": "srl_nokia-platform:platform/srl_nokia-platform-control:control[slot=A]",
              "values": {
                  "srl_nokia-platform:platform/srl_nokia-platform-control:control": {
                  "software-version": "v20.6.1-286-g118bc27b34"
                  }
              }
              }
          ]
          }

          {
          "source": "containerlab-t-leaf1:57400",
          "subscription-name": "bgp_neighbor_state",
          "timestamp": 1602192196569414357,
          "time": "2020-10-08T23:23:16.569414357+02:00",
          "updates": [
              {
              "Path": "srl_nokia-network-instance:network-instance[name=default]/protocols/srl_nokia-bgp:bgp/neighbor[peer-address=2002::100:64:2:0]",
              "values": {
                  "srl_nokia-network-instance:network-instance/protocols/srl_nokia-bgp:bgp/neighbor": {
                  "admin-state": "enable"
                  }
              }
              }
          ]
          }

          {
          "source": "containerlab-t-leaf1:57400",
          "subscription-name": "bgp_neighbor_state",
          "timestamp": 1602192196570053448,
          "time": "2020-10-08T23:23:16.570053448+02:00",
          "updates": [
              {
              "Path": "srl_nokia-network-instance:network-instance[name=default]/protocols/srl_nokia-bgp:bgp/neighbor[peer-address=2002::100:64:3:0]",
              "values": {
                  "srl_nokia-network-instance:network-instance/protocols/srl_nokia-bgp:bgp/neighbor": {
                  "admin-state": "enable"
                  }
              }
              }
          ]
          }
      ```    
      </details>
